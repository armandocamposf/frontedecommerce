import axios from 'axios'
import {useAuthStore} from '@/stores/useAuthStore'
const storeUser = useAuthStore()


const api = axios.create({
  baseURL: 'https://api.impresosgalarce.cl/api/',
  // baseURL: 'http://apiecommerce.test/api/',
  headers: {
    'Content-type': 'application/json',
    'Authorization': 'Bearer ' + storeUser.token
  },
})

export default api